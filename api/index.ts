import express, {
    Request,
    Response
} from 'express';
var axios = require('axios');
const app = express();
import dotenv from 'dotenv';
dotenv.config();
import logger from '../config/logger.config';
import {
    createClient
} from '@supabase/supabase-js';
import {
    VerificationMiddleware
} from '../middleware/middleware';
import {
    body
} from 'express-validator';
import {
    SingleBodyValidationMiddleware
} from '../middleware/common.middleware';

const supabaseUrl: any = process.env.PUBLIC_SUPABASE_URL !== undefined ? process.env.PUBLIC_SUPABASE_URL : '';
const supabaseKey: any = process.env.PUBLIC_SUPABASE_ANON_KEY !== undefined ? process.env.PUBLIC_SUPABASE_ANON_KEY : '';
const supabase = createClient(supabaseUrl, supabaseKey);


app.get('/', async (req: Request, res: Response) => {
    try {
        const {
            data,
            error
        } = await supabase.from('person').select().eq('id', 16);
        console.log("person_id 16 data: ", data, error);
        return res.json({
            status: 200,
            data: data
        });
    } catch (error) {
        console.error(error);
        return res.status(500).send("Server error");
    }
});

app.post('/trigger-email',
    VerificationMiddleware,
    body('domain').notEmpty().withMessage('Please enter a domain'),
    SingleBodyValidationMiddleware,
    async (req: Request, res: Response) => {
        try {
            const {
                data,
                error
            } = await supabase.from('campaign_status')
                .select("id, status, from, list_id, to_id(first_name, last_name, email)")
                .eq("status", "to_send")
                .order('added_on', {
                    ascending: true
                })
                .like('from', `%${req.body.domain}%`);

            if (error) throw new Error(error.message as string);
            let resultData;
            if (data !== undefined) {
                resultData = post_process_data(data);
            }
            // console.log("resultData: ", resultData);
            const config = {
                method: 'put',
                url: 'https://api.sendgrid.com/v3/marketing/contacts',
                headers: {
                    'Authorization': 'Bearer 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',
                    'Content-Type': 'application/json'
                },
                data: resultData
            };

            axios(config)
                .then(function (response: any) {
                    return res.json({
                        status: 200,
                        job_id: response.data.job_id
                    });
                })
                .catch(function (error: any) {
                    // console.log(error.message);
                    logger('error', 'POST', '/api/trigger-email', 400, error.message);
                    return res.status(400).json({
                        message: error.message
                    });
                });

        } catch (err: {
            message: string
        } | any) {
            // console.log(err.message);
            logger('error', 'POST', '/api/trigger-email', 500, 'Issue on Server. Resolving it.');
            return res.status(500).json({
                message: err.message
            });
        }
    });


const post_process_data = (result: any) => {
    // console.log("result", result);
    return {
        "list_ids": [
            result[0].list_id
        ],
        "contacts": [{
            "email": result[0].to_id.email,
            "first_name": result[0].to_id.first_name,
            "last_name": result[0].to_id.last_name !== undefined ? result[0].to_id.last_name : ''
        }]
    }
}


module.exports = app;