import {
    NextFunction,
    Request,
    Response
} from "express";
import validator from 'validator';
import logger from '../config/logger.config';
require('dotenv').config();

const isHeaderValid = (value: any): boolean =>
    value !== undefined && !validator.isEmpty(value) && value === process.env.SECRET_TOKEN;


export const VerificationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if(req.headers['dt-auth'] !== undefined && isHeaderValid(req.headers['dt-auth'])){
        next();
    } else {
        logger('error', req.method, req.path, 401, 'Invalid secret token');
        return res.status(401).json({"message": process.env.RESPONSE_UNAUTHORIZED});
    }
}