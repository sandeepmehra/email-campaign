import express from 'express';
const app = express();
require('dotenv').config();
const routes = require("./api/index");

app.use(express.json());

app.use("/api", routes);

const PORT = process.env.PORT || 8003;
app.listen(PORT, ()=> console.log(`Server is running in port  ${PORT}`));